import React from 'react';
import { Layout, Menu } from 'antd';
import { Link, Navigate, Route, Routes } from 'react-router-dom';
import routes from './routes';

const navigationLinks = [
  {
    label: 'Products',
    link: '/products',
  },
  {
    label: 'Create product',
    link: '/create',
  },
];

const App = () => {
  return (
    <Layout className="layout">
      <Layout.Header style={{position: 'fixed', zIndex: 2, width: '100%'}}>
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={[window.location.pathname]}
        >
          {navigationLinks.map(({label, link}) => (
            <Menu.Item key={link}>
              <Link to={link}>
                {label}
              </Link>
            </Menu.Item>
          ))}
        </Menu>
      </Layout.Header>
      <Layout.Content style={{padding: '24px 50px', marginTop: 64}}>
        <div className="layout-content">
          <Routes>
            <Route path="/" exact element={<Navigate to="/products" />} />
            {routes.map(({link, component}) => (
              <Route
                key={link}
                path={link}
                element={component}
              />
            ))}
          </Routes>
        </div>
      </Layout.Content>
      <Layout.Footer style={{textAlign: 'center'}}>
        Exore LTD.
      </Layout.Footer>
    </Layout>
  );
};

export default App;
