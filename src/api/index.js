import axios from 'axios';

const urlAPI = 'https://fakestoreapi.com';

class AxiosAPI {
  request = (endpoint, types = [], data, options = {}, method) => {
    const {
      onFailure,
      onSuccess,
      url,
    } = options;

    const [REQUEST, SUCCESS, FAILURE] = types;

    const headers = {
      'Content-Type': data instanceof FormData ?
        'multipart/form-data' :
        'application/json',
    };

    const config = {
      url: `${url || urlAPI}/${endpoint}`,
      method,
      data,
      headers,
    };

    return (dispatch) => {
      if (REQUEST) {
        dispatch({
          type: REQUEST.type || REQUEST,
          payload: REQUEST.payload || REQUEST,
        });
      }

      axios.request(config)
        .then((response) => {
          if (SUCCESS) {
            dispatch({
              type: SUCCESS.type || SUCCESS,
              payload: SUCCESS.payload ?
                SUCCESS.payload(response.data) :
                response.data,
            });
          }

          if (onSuccess) {
            onSuccess(response.data);
          }
        })
        .catch((error) => {
          if (FAILURE) {
            dispatch({
              type: FAILURE.type || FAILURE,
              payload: FAILURE.payload ?
                FAILURE.payload(error) :
                error,
            });
          }

          if (onFailure) {
            onFailure(error.message, error.status);
          }
        });
    };
  };

  get = (endpoint, types, options) => this.request(
    endpoint,
    types,
    null,
    options,
    'get',
  );

  post = (endpoint, types, data, options) => this.request(
    endpoint,
    types,
    data,
    options,
    'post',
  );

  patch = (endpoint, types, data, options) => this.request(
    endpoint,
    types,
    data,
    options,
    'patch',
  );

  delete = (endpoint, types, options) => this.request(
    endpoint,
    types,
    null,
    options,
    'delete',
  );
}

const api = new AxiosAPI();

export default api;
