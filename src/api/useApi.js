import api from './index';
import { productTypes } from '../constants/productTypes';
import { useDispatch } from 'react-redux';
import _ from 'lodash';

const useApi = () => {
  const dispatch = useDispatch();

  const requestGetProductsList = (limit) => new Promise((resolve, reject) => {
    dispatch(
      api.get(
        limit ? `products?limit=${limit}` : 'products',
        [
          {
            type: productTypes.GET_PRODUCTS_LIST_REQUEST,
            payload: limit,
          },
          productTypes.GET_PRODUCTS_LIST_SUCCESS,
          productTypes.GET_PRODUCTS_LIST_FAILURE,
        ],
        {
          onSuccess: resolve,
          onFailure: reject,
        },
      ),
    );
  });

  const requestGetProduct = (id) => new Promise((resolve, reject) => {
    dispatch(
      api.get(
        `products/${id}`,
        [
          productTypes.GET_PRODUCT_REQUEST,
          productTypes.GET_PRODUCT_SUCCESS,
          productTypes.GET_PRODUCT_FAILURE,
        ],
        {
          onSuccess: resolve,
          onFailure: reject,
        },
      ),
    );
  });

  const requestCreateProduct = (data) => new Promise((resolve, reject) => {
    dispatch(
      api.post(
        'products',
        [
          productTypes.CREATE_PRODUCT_REQUEST,
          {
            type: productTypes.CREATE_PRODUCT_SUCCESS,
            payload: () => ({id: _.toString(new Date().getTime()), ...data}),
          },
          productTypes.CREATE_PRODUCT_FAILURE,
        ],
        data,
        {
          onSuccess: resolve,
          onFailure: reject,
        },
      ),
    );
  });

  const requestUpdateProduct = (id, data) => new Promise((resolve, reject) => {
    dispatch(
      api.patch(
        `products/${id}`,
        [
          productTypes.UPDATE_PRODUCT_REQUEST,
          productTypes.UPDATE_PRODUCT_SUCCESS,
          productTypes.UPDATE_PRODUCT_FAILURE,
        ],
        data,
        {
          onSuccess: resolve,
          onFailure: reject,
        },
      ),
    );
  });

  const requestDeleteProduct = (id) => new Promise((resolve, reject) => {
    dispatch(
      api.delete(
        `products/${id}`,
        [
          productTypes.DELETE_PRODUCT_REQUEST,
          {
            type: productTypes.DELETE_PRODUCT_SUCCESS,
            payload: () => id,
          },
          {
            type: productTypes.DELETE_PRODUCT_FAILURE,
            payload: () => id,
          },
        ],
        {
          onSuccess: resolve,
          onFailure: reject,
        },
      ),
    );
  });

  return ({
    requestGetProductsList,
    requestGetProduct,
    requestCreateProduct,
    requestUpdateProduct,
    requestDeleteProduct,
  });
};

export default useApi;
