import { Button, Modal, notification } from 'antd';
import Icon from '@mdi/react';
import { mdiTrashCanOutline } from '@mdi/js';
import useApi from '../api/useApi';

const DeleteButton = ({
  id,
  title = '',
  showIcon,
  afterDelete,
}) => {
  const {requestDeleteProduct} = useApi();

  const showDeleteConfirm = () => {
    Modal.confirm({
      title: 'Are you sure delete this product?',
      content: 'Product was deleted permanently',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        requestDeleteProduct(id)
          .catch(() => {
            notification.success({
              message: 'Success!',
              description:
                'Product was deleted',
            });
            if (afterDelete) {
              afterDelete();
            }
          })
      },
    });
  };

  return (
    <Button
      type="primary"
      size="small"
      onClick={showDeleteConfirm}
      danger
      ghost
    >
      {showIcon && (
        <Icon
          path={mdiTrashCanOutline}
          size={0.7}
          className={title ? 'mr-2' : ''}
        />
      )}
      {title}
    </Button>
  );
};

export default DeleteButton;
