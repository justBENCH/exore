import { Button, Checkbox, Form, Input, InputNumber, Spin } from 'antd';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { getCreatedProductsLoading } from '../store/selectors';

const EditForm = ({
  submitTitle,
  onSubmit,
  data = null,
}) => {
  const [form] = Form.useForm();
  const loading = useSelector(getCreatedProductsLoading);

  const rules = [
    {
      required: true,
      message: 'This field is required!',
    },
  ];

  useEffect(() => {
    form.setFieldsValue(data);
  }, []);

  return (
    <Spin spinning={loading}>
      <Form
        layout="vertical"
        form={form}
        wrapperCol={{span: 12, offset: 6}}
        labelCol={{span: 12, offset: 6}}
        autoComplete="off"
        onFinish={onSubmit}
      >
        <Form.Item
          label="Title"
          name="title"
          rules={rules}
        >
          <Input placeholder="Enter title" />
        </Form.Item>

        <Form.Item
          label="Description"
          name="description"
          rules={rules}
        >
          <Input.TextArea placeholder="Enter description" />
        </Form.Item>
        <Form.Item
          label="Price $"
          name="price"
          rules={rules}
        >
          <InputNumber
            placeholder="Enter price"
            min="0"
            step="0.01"
            stringMode
            style={{minWidth: 140}}
          />
        </Form.Item>
        <Form.Item
          name="published"
          valuePropName="checked"
        >
          <Checkbox>Published</Checkbox>
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            style={{minWidth: 120}}
          >
            {submitTitle}
          </Button>
        </Form.Item>
      </Form>
    </Spin>
  );
};

export default EditForm;
