import { Button, Col, Row, Space } from 'antd';
import { useSelector } from 'react-redux';
import { getProductsListLimit } from '../store/selectors';

const btnPageSize = [
  {
    label: '8',
    value: 8,
  },
  {
    label: '16',
    value: 16,
  },
  {
    label: 'All',
    value: 'all',
  },
];

const PageSize = ({
  onChangeSize,
}) => {
  const limit = useSelector(getProductsListLimit);

  return (
    <Row>
      <Col span={24} className="d-flex justify-content-center mt-3">
        <Space size={[4, 0]}>
          {btnPageSize.map(({label, value}) => (
            <Button
              key={label}
              onClick={() => onChangeSize(value)}
              type="primary"
              disabled={value === limit}
            >
              {label}
            </Button>
          ))}
        </Space>
      </Col>
    </Row>
  )
}

export default PageSize;
