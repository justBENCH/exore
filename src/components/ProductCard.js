import { Card, Image } from 'antd';
import Icon from '@mdi/react';
import { mdiImageOffOutline } from '@mdi/js';

const ProductCard = ({
  data,
}) => {
  return (
    <Card
      hoverable
      className="product_card"
      cover={
        data.image ? (
          <Image
            alt="product cover"
            src={data.image}
            className="product_card-image"
            preview={false}
            style={{
              height: 250,
              objectFit: 'contain',
            }}
          />
        ) : (
          <div
            style={{height: 250}}
            className="d-flex flex-column align-items-center justify-content-center"
          >
            <Icon
              path={mdiImageOffOutline}
              size={3}
              className="mb-2"
              color="#8c8c8c"
            />
            No image
          </div>
        )
      }
    >
      <Card.Meta
        title={data.title}
        description={`${data.price}$`}
      />
    </Card>
  );
};

export default ProductCard;
