import ProductCard from './ProductCard';
import { Col, Row, Spin } from 'antd';
import { Link } from 'react-router-dom';

const ProductsList = ({
  data,
  loading = false,
}) => {
  return (
    <>
      <Row
        gutter={[12, 12]}
        style={{minHeight: 300}}
      >
        {data.map((data) => {
          const card = (
            <ProductCard data={data} />
          );

          return (
            <Col
              span={6}
              key={data.id}
            >
              <Link to={`/products/${data.id}`}>
                {card}
              </Link>
            </Col>
          );
        })}
      </Row>
      <Row>
        <Col
          span={24}
          className="d-flex justify-content-center"
        >
          <Spin
            spinning={loading}
            tip="Loading..."
          />
        </Col>
      </Row>
    </>
  );
};

export default ProductsList;
