import { Button, Space, Table } from 'antd';
import { Link } from 'react-router-dom';
import DeleteButton from './DeleteButton';

const ProductsTable = ({
  data,
  searchValue,
  onlyPublished,
}) => {
  const columns = [
    {
      dataIndex: 'id',
      title: 'ID',
    },
    {
      dataIndex: 'title',
      title: 'Title',
    },
    {
      dataIndex: 'description',
      title: 'Description',
    },
    {
      dataIndex: 'published',
      title: 'Published',
      render: (cell) => cell ? 'Yes' : 'No',
      sorter: (a, b) => +a.published - +b.published,
    },
    {
      dataIndex: 'price',
      title: 'Price',
      render: (cell) => `${cell}$`,
    },
    {
      dataIndex: 'id',
      key: 'actions',
      render: (cell) => (
        <Space size={[8, 0]}>
          <Link to={`/edit/${cell}`}>
            <Button
              type="primary"
              size="small"
              ghost
            >
              Edit
            </Button>
          </Link>
          <DeleteButton
            id={cell}
            title="Delete"
          />
        </Space>
      ),
    },
  ];

  const getFilteredData = () => {
    if (onlyPublished || searchValue) {
      return data.filter(({title, published}) => {
        if (onlyPublished && !published) {
          return false;
        }

        if (searchValue) {
          return title.match(new RegExp(searchValue, 'i'));
        }

        return true;
      });
    }

    return data;
  };

  return (
    <Table
      dataSource={getFilteredData()}
      rowKey={'id'}
      columns={columns}
    />
  );
};

export default ProductsTable;
