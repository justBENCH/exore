import keyMirror from 'keymirror';

export const productTypes = keyMirror({
  GET_PRODUCTS_LIST_REQUEST: null,
  GET_PRODUCTS_LIST_SUCCESS: null,
  GET_PRODUCTS_LIST_FAILURE: null,

  GET_PRODUCT_REQUEST: null,
  GET_PRODUCT_SUCCESS: null,
  GET_PRODUCT_FAILURE: null,

  CREATE_PRODUCT_REQUEST: null,
  CREATE_PRODUCT_SUCCESS: null,
  CREATE_PRODUCT_FAILURE: null,

  UPDATE_PRODUCT_REQUEST: null,
  UPDATE_PRODUCT_SUCCESS: null,
  UPDATE_PRODUCT_FAILURE: null,

  DELETE_PRODUCT_REQUEST: null,
  DELETE_PRODUCT_SUCCESS: null,
  DELETE_PRODUCT_FAILURE: null,
})
