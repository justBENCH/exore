import useApi from '../api/useApi';
import EditForm from '../components/EditForm';
import { notification } from 'antd';

const CreatePage = () => {
  const { requestCreateProduct } = useApi();

  const onCreateProduct = (data) => {
    const created = new Date();
    requestCreateProduct({...data, created})
      .then(() => {
        notification.success({
          message: 'Success!',
          description:
            'Product was created!',
        });
      })
  }

  return (
    <EditForm
      submitTitle="Save"
      onSubmit={onCreateProduct}
    />
  );
};

export default CreatePage;
