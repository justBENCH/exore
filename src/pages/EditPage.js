import { useNavigate, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getProductEdited } from '../store/selectors';
import EditForm from '../components/EditForm';
import useApi from '../api/useApi';
import { Col, notification, Row } from 'antd';
import DeleteButton from '../components/DeleteButton';

const EditPage = () => {
  const {requestUpdateProduct} = useApi();
  const {id: productID} = useParams();
  const navigate = useNavigate()

  const productData = useSelector((state) => getProductEdited(
    state,
    productID,
  ));

  const onUpdateProduct = (data) => {
    requestUpdateProduct(productID, data)
      .then(() => {
        notification.success({
          message: 'Success!',
          description:
            'Product was edited!',
        });
      })
  };

  return (
    <Row>
      <Col span={24}>
        <DeleteButton
          id={productID}
          title="Delete"
          type="primary"
          afterDelete={() => navigate('/')}
        />
      </Col>
      <Col span={24}>
        <EditForm
          submitTitle="Save"
          onSubmit={onUpdateProduct}
          data={productData}
        />
      </Col>
    </Row>
  );
};

export default EditPage;
