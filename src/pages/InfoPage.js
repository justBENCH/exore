import { Col, Descriptions, Image, Rate, Row, Spin, Typography } from 'antd';
import useApi from '../api/useApi';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getProductDetail, getProductDetailLoading } from '../store/selectors';

const InfoPage = () => {
  const {requestGetProduct} = useApi();
  const {id: productID} = useParams();

  const productData = useSelector(getProductDetail);
  const loading = useSelector(getProductDetailLoading);

  useEffect(() => {
    if (productID) {
      requestGetProduct(productID);
    }
  }, []);

  return (
    <Row gutter={[24, 0]}>
      {!productData || loading ? (
        <Col
          span={24}
          className="d-flex justify-content-center"
        >
          <Spin
            size="large"
            tip="Loading..."
          />
        </Col>
      ) : (
        <>
          <Col span={6} className="d-flex align-items-center">
            <Image
              src={productData.image}
              style={{
                width: '100%',
                // height: 250,
                objectFit: 'contain',
              }}
            />
          </Col>
          <Col span={18}>
            <Descriptions
              column={1}
              bordered
            >
              <Descriptions.Item label="Title">
                <Typography.Title level={4}>
                  {productData.title}
                </Typography.Title>
              </Descriptions.Item>
              <Descriptions.Item label="Description">{productData.description}</Descriptions.Item>
              <Descriptions.Item label="Category">{productData.category}</Descriptions.Item>
              <Descriptions.Item label="Rating">
                <Rate disabled allowHalf defaultValue={productData.rating.rate} />
                <span className="ml-2">
                  {`(${productData.rating.count})`}
                </span>
              </Descriptions.Item>
            </Descriptions>
          </Col>
        </>
      )}
    </Row>
  );
};

export default InfoPage;
