import { Checkbox, Input, Tabs } from 'antd';
import ProductsList from '../components/ProductsList';
import PageSize from '../components/PageSize';
import { useSelector } from 'react-redux';
import {
  getCreatedProductsList,
  getProductsList,
  getProductsListLimit,
  getProductsListLoading,
} from '../store/selectors';
import useApi from '../api/useApi';
import { useEffect, useState } from 'react';
import ProductsTable from '../components/ProductsTable';

const MainPage = () => {
  const {requestGetProductsList} = useApi();

  const downloadedProducts = useSelector(getProductsList);
  const createdProducts = useSelector(getCreatedProductsList);
  const loading = useSelector(getProductsListLoading);
  const limit = useSelector(getProductsListLimit);

  const [onlyPublished, setOnlyPublished] = useState(false);
  const [searchValue, setSearchValue] = useState('');

  const publishedFilter = (
    <Checkbox
      onChange={(e) => setOnlyPublished(e.target.checked)}
    >
      Only published
    </Checkbox>
  );

  const downloadProductsList = (size) => {
    if (size !== limit) {
      requestGetProductsList(size);
    }
  };

  const onSearchCreated = (value) => {
    setSearchValue(value);
  }

  useEffect(() => {
    if (downloadedProducts.length === 0) {
      downloadProductsList(8);
    }
  }, []);

  return (
    <Tabs
      defaultActiveKey="1"
      tabBarExtraContent={publishedFilter}
    >
      <Tabs.TabPane
        tab="All products"
        key="all"
      >
        <ProductsList
          data={downloadedProducts}
          loading={loading}
        />
        <PageSize
          onChangeSize={downloadProductsList}
          limit={limit}
        />
      </Tabs.TabPane>
      <Tabs.TabPane
        tab="Created products"
        key="created"
      >
        <Input.Search
          placeholder="Enter product title for search"
          allowClear
          onSearch={onSearchCreated}
          style={{width: 350}}
          className="mb-3"
        />
        <ProductsTable
          data={createdProducts}
          onlyPublished={onlyPublished}
          searchValue={searchValue}
        />
      </Tabs.TabPane>
    </Tabs>
  );
};

export default MainPage;
