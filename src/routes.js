import InfoPage from './pages/InfoPage';
import CreatePage from './pages/CreatePage';
import MainPage from './pages/MainPage';
import EditPage from './pages/EditPage';

const routes = [
  {
    link: '/products',
    component: <MainPage />,
  },
  {
    link: '/products/:id',
    component: <InfoPage />,
  },
  {
    link: '/create',
    component: <CreatePage />,
  },
  {
    link: '/edit/:id',
    component: <EditPage />,
  },
];

export default routes;
