import { productTypes } from '../constants/productTypes';
import { combineReducers } from '@reduxjs/toolkit';

const downloadedReducer = (
  state = {
    data: [],
    loading: false,
    limit: null,
  }, {payload, type},
) => {
  switch (type) {
    case productTypes.GET_PRODUCTS_LIST_REQUEST:
      return {
        ...state,
        limit: payload,
        loading: true,
      };

    case productTypes.GET_PRODUCTS_LIST_SUCCESS:
      return {
        ...state,
        data: payload,
        loading: false,
      };

    default:
      return state;
  }
};

const selectedReducer = (
  state = {
    data: null,
    loading: false,
  }, {payload, type},
) => {
  switch (type) {
    case productTypes.GET_PRODUCT_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case productTypes.GET_PRODUCT_SUCCESS:
      return {
        ...state,
        data: payload,
        loading: false,
      };

    default:
      return state;
  }
};

const createdReducer = (
  state = {
    data: [],
    loading: false,
  }, {payload, type},
) => {
  switch (type) {
    case productTypes.CREATE_PRODUCT_REQUEST:
    case productTypes.DELETE_PRODUCT_REQUEST:
    case productTypes.UPDATE_PRODUCT_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case productTypes.CREATE_PRODUCT_SUCCESS:
      return {
        ...state,
        data: [...state.data, payload],
        loading: false,
      };

    case productTypes.DELETE_PRODUCT_SUCCESS:
    case productTypes.DELETE_PRODUCT_FAILURE:
      return {
        ...state,
        data: state.data.filter(({id}) => payload !== id),
        loading: false,
      };

    case productTypes.UPDATE_PRODUCT_SUCCESS:
      return {
        ...state,
        data: [...state.data].map((item) => item.id === +payload.id ?
          payload :
          item),
        loading: false,
      };

    default:
      return state;
  }
};

const rootReducer = combineReducers({
  downloaded: downloadedReducer,
  created: createdReducer,
  selected: selectedReducer,
});

export default rootReducer;
