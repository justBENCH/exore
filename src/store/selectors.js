import _ from 'lodash';

export const getProductsList = (state) => _.get(
  state,
  'downloaded.data',
);
export const getProductsListLimit = (state) => _.get(
  state,
  'downloaded.limit',
);
export const getProductsListLoading = (state) => _.get(
  state,
  'downloaded.loading',
);

export const getProductDetail = (state) => _.get(
  state,
  'selected.data',
);

export const getProductDetailLoading = (state) => _.get(
  state,
  'selected.loading',
);

export const getCreatedProductsList = (state) => _.get(
  state,
  'created.data',
);

export const getCreatedProductsLoading = (state) => _.get(
  state,
  'created.loading',
);

export const getProductEdited = (state, id) => {
  const createdList = getCreatedProductsList(state);

  return _.find(createdList, (item) => +item.id === +id);
};
